## Info

This is the module for the CloudNet-Interface, which you have to put in your
modules-folder in the CloudNet-Master.

## Using

The interface and module are far from beeing finished and there are not many features
implemented yet, but if you still want to use them and test the current features,
you can download the latest builds [here](https://gitlab.com/cloudnet-interface/cloudnet-interface-extension/-/jobs/artifacts/master/download?job=verify).
You can download the code and run this command in the directory of where you saved it, too: 

```bash
$ mvn package
```

Notice: You need [Maven](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html) for this.


When you start the cloud now, you'll see, that there's a new folder called "Interface-Extension"
in the modules-folder, in which you can find a file named "config.yml". In this file are two variables.

The "tokenSecret" is only used for a safe auto-login from the interface. It's better
when you don't change it and keep it as a secret.
The "webSocketServerPort" is the port where the socketServer for the interface-connection
will run on. You can change it if you want or if there's another process running 
on this port.

Now if you start your master, you'll see, that the address where the socketServer
is running on has been printed to the console. You use this address to connect to
CloudNet later from the interface.

But first, you have to create an user, with which you'll connect to the interface later.
For this, type the following into the master console:

```bash
CREATE USER <Name> <Password>
```

You have to notice the password, it will be saved hashed. Now you have to give the user 
some permissions, so that he can manage the cloud. For this, open the file "users.json"
in the directory of the master. There, search the object for your user and add permissions
to the "permissions"-array. I will put a full list of all permissions here later, for now, 
add the permission "*", which means full permissions. It should look like this:

```json
"permissions": [
    "*"
]
```

That's all. Now you can login in the interface with the address printed in the master-console,
the name and the password of your user you just created. Have fun ;)

