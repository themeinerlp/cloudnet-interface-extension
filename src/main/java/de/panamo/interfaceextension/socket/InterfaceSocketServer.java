package de.panamo.interfaceextension.socket;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.gson.JsonSyntaxException;
import de.dytanic.cloudnet.lib.user.User;
import de.dytanic.cloudnet.lib.utility.document.Document;
import de.dytanic.cloudnet.lib.utility.threading.Scheduler;
import de.panamo.interfaceextension.InterfaceExtension;
import de.panamo.interfaceextension.ModuleConfiguration;
import de.panamo.interfaceextension.messagehandler.MessageHandler;
import org.java_websocket.WebSocket;
import org.java_websocket.framing.CloseFrame;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class InterfaceSocketServer extends WebSocketServer {
    private static final String PREFIX = "[WebSocketServer] ";
    private InterfaceExtension instance;
    private Map<WebSocket, String> authorizedSockets = new HashMap<>();
    private List<MessageHandler> messageHandlers = new ArrayList<>();
    private Algorithm tokenAlgorithm;
    private JWTCreator.Builder jwtBuilder;
    private JWTVerifier jwtVerifier;

    public InterfaceSocketServer(InterfaceExtension instance, ModuleConfiguration moduleConfiguration) {
        super(new InetSocketAddress(instance.getCloud().getConfig().getWebServerConfig().getAddress(),
                moduleConfiguration.getPort()));
        this.instance = instance;
        this.tokenAlgorithm = Algorithm.HMAC256(moduleConfiguration.getTokenSecret());
        this.jwtBuilder = JWT.create().withIssuer("cloudNetInterface");
        this.jwtVerifier = JWT.require(this.tokenAlgorithm).withIssuer("cloudNetInterface").build();
    }


    /**
     * Sends a notification that will show in all connected interfaces
     *
     * @param message the notification-message
     */

    public void broadcastNotification(String message) {
        Document responseDocument = new Document("message", "notification").append("content", message);
        super.broadcast(responseDocument.convertToJsonString());
    }

    /**
     * Registers a messageHandler, which will be called when receiving a message from a socket
     *
     * @param messageHandler the handler
     */

    public void registerHandler(MessageHandler messageHandler) {
        this.messageHandlers.add(messageHandler);
    }

    /**
     * Handles the start of the server
     */

    @Override
    public void onStart() {
        System.out.println(PREFIX + "Successfully bound WebSocketServer to " + super.getAddress().toString());
        Scheduler scheduler = this.instance.getCloud().getScheduler();
        scheduler.runTaskRepeatSync(() -> {
            for(WebSocket webSocket : super.getConnections()) {
                if(!this.authorizedSockets.containsKey(webSocket)) {
                    System.out.println(PREFIX + "Closing WebSocket " + webSocket.getRemoteSocketAddress().toString()
                            + " because of missing authentication");
                    webSocket.close(CloseFrame.NORMAL, "Permission denied");
                }
            }
        }, scheduler.getTicks(), scheduler.getTicks() * 10);

    }

    /**
     * Handles a new socket-connection
     *
     * @param webSocket the socket
     * @param clientHandshake the handshake
     */

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        System.out.println("New WebSocket from " + webSocket.getRemoteSocketAddress().toString());
    }

    /**
     * Handles messages from a socket
     *
     * @param webSocket the socket
     * @param jsonMessage the message
     */

    @Override
    public void onMessage(WebSocket webSocket, String jsonMessage) {
        Document document = Document.load(jsonMessage);
        if(document.getString("message") != null) {
            String message = document.getString("message").toLowerCase();
            String[] values = document.getObject("values", String[].class);

            Document responseDocument = new Document().append("message", document.getString("message")).append("response", new Document());

            // Handle auth
            if(message.equals("auth") && values.length > 1) {
                String user = values[0];
                String password = values[1];
                if (this.instance.getCloud().authorizationPassword(user, password)) {
                    // Add socket to the authorized sockets
                    System.out.println(PREFIX + "WebSocket " + webSocket.getRemoteSocketAddress().toString() + " authorized successfully "
                            + "with user " + user);
                    this.authorizedSockets.put(webSocket, user);
                    responseDocument.append("response", Arrays.asList(this.createUserToken(user), this.instance.getVersion()));
                } else {
                    webSocket.close(CloseFrame.NORMAL, "Permission denied");
                    return;
                }
            } else if(message.equals("auth-token") && values.length == 1) {
                String user = this.verifyToken(values[0]);
                if (user != null) {
                    // Add socket to the authorized sockets
                    System.out.println(PREFIX + "WebSocket " + webSocket.getRemoteSocketAddress().toString() + " authorized successfully "
                            + "with user " + user);
                    this.authorizedSockets.put(webSocket, user);
                    responseDocument.append("response", this.instance.getVersion());
                } else {
                    webSocket.close(CloseFrame.NORMAL, "Permission denied");
                    return;
                }
            } else if(!this.authorizedSockets.containsKey(webSocket)) {
                webSocket.close(CloseFrame.NORMAL, "Permission denied");
                return;
            } else
                this.handleSuccessfulMessage(message, values, responseDocument, webSocket,
                        this.instance.getCloud().getUser(this.authorizedSockets.get(webSocket)));
            webSocket.send(responseDocument.convertToJsonString());
        }
    }

    /**
     * Handles an error which happened because of a certain socket
     *
     * @param webSocket the socket
     * @param exception the error
     */

    @Override
    public void onError(WebSocket webSocket, Exception exception) {
        if(exception == null)
            return;
        if(webSocket == null)
            System.err.println(PREFIX + "Error at the WebSocketServer: " + exception.getMessage());
        else {
            System.err.println(PREFIX + "Error with WebSocket " + webSocket.getRemoteSocketAddress().toString() +
                    ": " + exception.getMessage());
            if(exception instanceof JsonSyntaxException)
                webSocket.close(CloseFrame.REFUSE, "Wrong JSON");
        }
        exception.printStackTrace();

    }

    /**
     * Handles a disconnection of a certain socket
     *
     * @param webSocket the socket
     * @param code the closeCode
     * @param reason the reason
     * @param remote remote
     */

    @Override
    public void onClose(WebSocket webSocket, int code, String reason, boolean remote) {
        System.out.println(PREFIX + "WebSocket from " +  webSocket.getRemoteSocketAddress().toString() +
                " disconnected with reason: " + reason + " (Code " + code + ")");
    }

    /**
     * Creates an authentication token for an user, which expires in 3 days
     *
     * @param user the user
     * @return the token
     */

    private String createUserToken(String user) {
        return this.jwtBuilder.withClaim("user", user).withExpiresAt(
                new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(3))).sign(this.tokenAlgorithm);
    }

    /**
     * Verifies a token
     *
     * @param token the token
     * @return the user of the token or null, if the token is invalid
     */

    private String verifyToken(String token) {
        try {
            DecodedJWT decodedJWT = this.jwtVerifier.verify(token);
            Claim claim = decodedJWT.getClaim("user");
            return claim.asString();
        } catch (JWTVerificationException exception) {
            System.out.println("Invalid token");
            return null;
        }
    }

    /**
     * Checks if an user has a certain permission
     *
     * @param user the user
     * @param permission the permission
     * @return if the user has permission
     */

    private boolean checkUserPermission(User user, String permission) {
        PermissionCategory permissionCategory = PermissionCategory.getFromPermission(permission);
        if(permissionCategory == null)
            return false;
        if(user.hasPermission(permissionCategory.getCategoryPermission()) || user.hasPermission("*"))
            return !this.getNegativePermissions(user).contains(permission);
        return user.hasPermission(permission);
    }

    /**
     * Returns negative permissions of a certain user
     *
     * @param user the user
     * @return the negative permissions as normal permissions
     */

    private Collection<String> getNegativePermissions(User user) {
        ArrayList<String> negativePermissions = new ArrayList<>();
        for(String permission : user.getPermissions()) {
            if (permission.startsWith("-"))
                negativePermissions.add(permission.substring(1, permission.length() -1).toLowerCase());
        }
        return negativePermissions;
    }

    /**
     * Handles a message from an authenticated webSocket
     *
     * @param message the message
     * @param values values of the message
     * @param responseDocument the document which is been sent as the response
     * @param user the cloudNet-user of the webSocket
     */

    private void handleSuccessfulMessage(String message, String[] values, Document responseDocument, WebSocket webSocket, User user) {
        if(!checkUserPermission(user, "cloudnet.web." + message)) {
            responseDocument.append("response", "Permission denied");
            return;
        }
        for(MessageHandler handler : this.messageHandlers)
            handler.handle(message, values, responseDocument, webSocket, user);
    }

    public enum PermissionCategory {
        INFO_PERMISSIONS("cloudnet.web.info", "cloudnet.web.serverinfos", "cloudnet.web.proxyinfos", "cloudnet.web.userinfo",
                "cloudnet.web.onlineplayers", "cloudnet.web.statistics", "cloudnet.web.cloudnetwork", "cloudnet.web.memory",
                "cloudnet.web.cpuusage", "cloudnet.web.startconsolesession", "cloudnet.web.stopconsolesession"),
        START_STOP_PERMISSIONS("cloudnet.web.start.stop", "cloudnet.web.startserver", "cloudnet.web.startproxy",
                "cloudnet.web.stopserver", "cloudnet.web.stopproxy"),
        CREATE_DELETE_PERMISSIONS("cloudnet.web.create.delete", "cloudnet.web.createservergroup", "cloudnet.web.createproxygroup",
                "cloudnet.web.createwrapper", "cloudnet.web.deleteservergroup", "cloudnet.web.deleteproxygroup"),
        CONSOLE("cloudnet.web.console", "cloudnet.web.serverconsolesession", "cloudnet.web.proxyconsolesession",
                "cloudnet.web.servercommand", "cloudnet.web.proxycommand");

        private String categoryPermission;
        private Collection<String> permissions;

        PermissionCategory(String categoryPermission, String... permissions) {
            this.categoryPermission = categoryPermission;
            this.permissions = Arrays.asList(permissions);
        }

        public String getCategoryPermission() {
            return categoryPermission;
        }

        public Collection<String> getPermissions() {
            return permissions;
        }

        public static PermissionCategory getFromPermission(String permission) {
            for(PermissionCategory permissionCategory : values()) {
                if(permissionCategory.getPermissions().contains(permission.toLowerCase()))
                    return permissionCategory;
            }
            return null;
        }
    }

}
